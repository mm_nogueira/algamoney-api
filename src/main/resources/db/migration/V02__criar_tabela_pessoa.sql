CREATE TABLE person (	
	id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	address VARCHAR(50),
	number VARCHAR(50),
	district VARCHAR(50),
	zipcode VARCHAR(50),
	city VARCHAR(50),
	state VARCHAR(50),
	enabled char(1) NOT NULL
)  
ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO person (name, address, number, district, zipcode, city, state, enabled) 
values ('Maurício Nogueira', 'Rua Manuel Isidoro Brenhas', '63', 'Vila Nova Jaraguá', '05180-380', 'São Paulo', 'SP', '1');

INSERT INTO person (name, address, number, district, zipcode, city, state, enabled) 
values ('Bruce Wayne', 'Rua Gothan', '100', 'BatCity', '01020-380', 'Sã	o Paulo', 'SP', '1');