package com.example.algamoney.api.resource;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.algamoney.api.event.ResourceCreatedEvent;
import com.example.algamoney.api.model.Person;
import com.example.algamoney.api.repository.PersonRepository;
import com.example.algamoney.api.service.PersonService;
import com.example.algamoney.api.vo.PersonParamVo;
import com.example.algamoney.api.vo.PersonVo;

@RestController
@RequestMapping("/persons")
public class PersonResource {

	@Autowired
	private PersonRepository personRepository;

	@Autowired
	private ApplicationEventPublisher publisher;

	@Autowired
	private PersonService personService;

//	@GetMapping
//	//@ApiOperation(value = "Consultar cadastro de pessoas", nickname = "search person")
//	public Page<PersonVo> search(@ModelAttribute final PersonParamVo params) {
//	//	log.debug("BEGIN search params: {}", params);
//		final Page<PersonVo> response = this.personService.search(params);
//	//	log.debug("END search response: {} person", response.getObjects().size());
//		return response;
//	}
//	
	
	@GetMapping
	public List<Person> list() {
		return personRepository.findAll();
	}

	@PostMapping
	public ResponseEntity<Person> create(@Valid @RequestBody Person person, HttpServletResponse response) {
		Person personSave = personRepository.save(person);
		publisher.publishEvent(new ResourceCreatedEvent(this, response, personSave.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(personSave);
	}

	@GetMapping("/{id}")
	public ResponseEntity findById(@PathVariable Long id) {
		Optional person = this.personRepository.findById(id);
		return person.isPresent() ? ResponseEntity.ok(person.get()) : ResponseEntity.notFound().build();
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void removeById(@PathVariable Long id) {
		personRepository.deleteById(id);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Person> update(@PathVariable Long id, @Valid @RequestBody Person person) {
		Person personSave = personService.updatePerson(id, person);
		return ResponseEntity.ok(personSave);
		
	}
	
	@PutMapping("/{id}/ativo")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateAtributeEnabled(@PathVariable Long id, @RequestBody Boolean enabled) {
		personService.updateAtributeEnabled(id, enabled);
		
	}
}
