package com.example.algamoney.api.resource;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.algamoney.api.event.ResourceCreatedEvent;
import com.example.algamoney.api.exceptionhandler.AlgamoneyExceptionHandler.Error;
import com.example.algamoney.api.model.Posting;
import com.example.algamoney.api.repository.PostingRepository;
import com.example.algamoney.api.repository.filter.PostingFilter;
import com.example.algamoney.api.service.PostingService;
import com.example.algamoney.api.service.exception.PersonNotFoundOrDisabledException;

@RestController
@RequestMapping("/postings")
public class PostingResource {

	@Autowired
	private PostingRepository postingRepository;

	@Autowired
	private ApplicationEventPublisher publisher;

	@Autowired
	private PostingService postingService;

	@Autowired
	private MessageSource messageSource;
	
	@GetMapping
	public Page<Posting> search(PostingFilter postingFilter, Pageable pageable) {
		return postingService.filter(postingFilter, pageable);
	}

	@GetMapping("/{id}")
	public ResponseEntity findById(@PathVariable Long id) {
		Optional posting = this.postingRepository.findById(id);
		return posting.isPresent() ? ResponseEntity.ok(posting.get()) : ResponseEntity.notFound().build();
	}
	
	@PostMapping
	public ResponseEntity<Posting> create(@Valid @RequestBody Posting posting, HttpServletResponse response) {
		Posting postingSave = postingService.createPosting(posting);
		publisher.publishEvent(new ResourceCreatedEvent(this, response, postingSave.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(postingSave);
	}
	
	@ExceptionHandler({PersonNotFoundOrDisabledException.class})
	public ResponseEntity<Object> handlePersonNotFoundOrDisabledException(PersonNotFoundOrDisabledException ex){
		
		String msgUser = messageSource.getMessage("pessoa.inexistente-ou-inativa", null, LocaleContextHolder.getLocale());
		String msgDev = ex.toString();
		List<Error> errors = Arrays.asList(new Error(msgUser, msgDev));

		return ResponseEntity.badRequest().body(errors);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void removeById(@PathVariable Long id) {
		postingRepository.deleteById(id);
	}

}
