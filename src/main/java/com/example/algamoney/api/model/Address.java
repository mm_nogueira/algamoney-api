package com.example.algamoney.api.model;

import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class Address {

	private String address;
	private String number;
	private String district;
	private String zipcode;
	private String city;
	private String state;
	
}
