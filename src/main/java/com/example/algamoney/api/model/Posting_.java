package com.example.algamoney.api.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Posting.class)
public abstract class Posting_ {

	public static volatile SingularAttribute<Posting, Person> person;
	public static volatile SingularAttribute<Posting, LocalDate> dueDate;
	public static volatile SingularAttribute<Posting, String> name;
	public static volatile SingularAttribute<Posting, Long> id;
	public static volatile SingularAttribute<Posting, LocalDate> paymentDate;
	public static volatile SingularAttribute<Posting, PostingType> type;
	public static volatile SingularAttribute<Posting, Category> category;
	public static volatile SingularAttribute<Posting, BigDecimal> value;
	public static volatile SingularAttribute<Posting, String> remarks;

	public static final String PERSON = "person";
	public static final String DUE_DATE = "dueDate";
	public static final String NAME = "name";
	public static final String ID = "id";
	public static final String PAYMENT_DATE = "paymentDate";
	public static final String TYPE = "type";
	public static final String CATEGORY = "category";
	public static final String VALUE = "value";
	public static final String REMARKS = "remarks";

}

