package com.example.algamoney.api.service;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.algamoney.api.model.Person;
import com.example.algamoney.api.model.Posting;
import com.example.algamoney.api.repository.PersonRepository;
import com.example.algamoney.api.repository.PostingRepository;
import com.example.algamoney.api.repository.filter.PostingFilter;
import com.example.algamoney.api.service.exception.PersonNotFoundOrDisabledException;

@Service
public class PostingService {

	@Autowired
	private PostingRepository postingRepository;

	@Autowired
	private PersonRepository personRepository;
	
	
	public Page<Posting> filter(PostingFilter postingFilter, Pageable pageable){
		Page<Posting> post =  postingRepository.filter(postingFilter, pageable);
		return post;
	}
	
	
	public Posting createPosting(@Valid Posting posting) {
		
		Optional<Person> personPosting = personRepository.findById(posting.getPerson().getId());
		if(!personPosting.isPresent() || personPosting.get().isDisabled()) {
			throw new PersonNotFoundOrDisabledException();
		}
		return postingRepository.save(posting);
	}
}
