package com.example.algamoney.api.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.example.algamoney.api.converter.PersonConverter;
import com.example.algamoney.api.model.Person;
import com.example.algamoney.api.repository.PersonRepository;
import com.example.algamoney.api.repository.filter.PersonFilter;
import com.example.algamoney.api.repository.specs.PersonSpec;
import com.example.algamoney.api.vo.PersonParamVo;
import com.example.algamoney.api.vo.PersonVo;
import lombok.extern.log4j.Log4j2;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class PersonService {

	@Autowired
	private PersonRepository personRepository;
	
	
	@Transactional(readOnly = true)
	public Page<PersonVo> search(final PersonParamVo params) {
		Assert.notNull(params, "params não pode ser nulo");
		log.debug("BEGIN search: params {}", params);

		final PersonFilter filter = PersonConverter.paramsToFilter(params);
		final int offset = Optional.ofNullable(params.getOffset()).orElse(PersonParamVo.DEFAULT_OFFSET);
		final int limit = Optional.ofNullable(params.getLimit()).orElse(PersonParamVo.DEFAULT_LIMIT);
		final Pageable pageable = PageRequest.of(offset, limit, new Sort(Sort.Direction.ASC, "id"));

		final Specification<Person> personSpec = PersonSpec.byUserFilter(filter, "id");
		final Page<Person> persons = this.personRepository.findAll(personSpec, pageable);

		final List<PersonVo> personVo = PersonConverter.modelToVo(persons.getContent());

//		final Paging paging = new Paging(offset, limit, users.getTotalElements(), users.getTotalPages());
//		final PagedResponseWrapper<AdmLegalMessageVo> userVoWrapper = new PagedResponseWrapper<>(mes, paging);

		log.debug("END search.");
		return null;
	}
	
	public Person updatePerson (Long id, Person person) {
		
		Optional<Person> personSave = foundPersonById(id);
		BeanUtils.copyProperties(person, personSave.get(), "id");
		
		return personRepository.save(personSave.get()); 
	}

	public void updateAtributeEnabled(Long id, Boolean enabled) {
		Optional<Person> personSave = foundPersonById(id);
		personSave.get().setEnabled(enabled);
		personRepository.save(personSave.get());
	}
	
	private Optional<Person> foundPersonById(Long id) {
		Optional<Person> personSave = this.personRepository.findById(id);
		if(!personSave.isPresent()) {
			throw new EmptyResultDataAccessException(1);
		}
		return personSave;
	}


}
