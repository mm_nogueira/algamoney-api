package com.example.algamoney.api.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.example.algamoney.api.model.Person;
import com.example.algamoney.api.repository.filter.PersonFilter;
import com.example.algamoney.api.vo.PersonParamVo;
import com.example.algamoney.api.vo.PersonVo;


public class PersonConverter {


    public static PersonVo modelToVo(final Person entity) {
        Assert.notNull(entity, "Objeto não pode ser nulo!");

        return PersonVo
				.builder()
					.id(entity.getId())
				.build();
    }

    public static Person voToModel(final PersonVo vo) {
        Assert.notNull(vo, "Objeto não pode ser nulo!");
        return Person
				.builder()
					.id(vo.getId())
					.name(vo.getName())
					.build();
    }


    public static List<PersonVo> modelToVo(final List<Person> listEntity) {
        if (CollectionUtils.isEmpty(listEntity)) {
            return new ArrayList<>();
        }

        return listEntity.stream().map(PersonConverter::modelToVo).collect(Collectors.toList());
    }
    

    public static List<Person> voToModel(final List<PersonVo> listVo) {
        if (CollectionUtils.isEmpty(listVo)) {
            return new ArrayList<>();
        }

        return listVo.stream().map(PersonConverter::voToModel).collect(Collectors.toList());
    }
	
    public static PersonFilter paramsToFilter(final PersonParamVo params) {
        Assert.notNull(params, "Parâmetros não pode ser nulo");
        return PersonFilter.builder()
            .id(params.getId())
            .name(params.getName())
            .build();
    }
	
}
