package com.example.algamoney.api.repository.posting;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import com.example.algamoney.api.model.Posting;
import com.example.algamoney.api.model.Posting_;
import com.example.algamoney.api.repository.filter.PostingFilter;


public class PostingRepositoryImpl implements PostingRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<Posting> filter(PostingFilter postingFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Posting> criteria = builder.createQuery(Posting.class);
		Root<Posting> root = criteria.from(Posting.class);
		
		Predicate[] predicates = criarRestricoes(postingFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<Posting> query = manager.createQuery(criteria);
		
		addCreatePageRestriction(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(postingFilter)) ;
	}

	private Predicate[] criarRestricoes(PostingFilter postingFilter, CriteriaBuilder builder,
			Root<Posting> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		if (!StringUtils.isEmpty(postingFilter.getName())) {
			predicates.add(builder.like(
					builder.lower(root.get(Posting_.name)), "%" + postingFilter.getName().toLowerCase() + "%"));
		}
		
		if (postingFilter.getDueDateStart() != null) {
			predicates.add(
					builder.greaterThanOrEqualTo(root.get(Posting_.dueDate), postingFilter.getDueDateStart()));
		}
		
		if (postingFilter.getDueDateEnd() != null) {
			predicates.add(
					builder.lessThanOrEqualTo(root.get(Posting_.dueDate), postingFilter.getDueDateEnd()));
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private Long total(PostingFilter postingFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Posting> root = criteria.from(Posting.class);
		
		Predicate[] predicates = criarRestricoes(postingFilter, builder, root);
		criteria.where(predicates);
		criteria.select(builder.count(root));
		
		return manager.createQuery(criteria).getSingleResult();
	}

	private void addCreatePageRestriction(TypedQuery<Posting> query, Pageable pageable) {
		int actualPage = pageable.getPageNumber();
		int totalRowsPerPage = pageable.getPageSize();
		int firstRowPage = actualPage * totalRowsPerPage;
		query.setFirstResult(firstRowPage);
		query.setMaxResults(totalRowsPerPage);
	}

}	
