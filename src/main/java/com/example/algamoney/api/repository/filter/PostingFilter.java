package com.example.algamoney.api.repository.filter;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

public class PostingFilter {

	private String name;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dueDateStart;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dueDateEnd;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getDueDateStart() {
		return dueDateStart;
	}

	public void setDueDateStart(LocalDate dueDateStart) {
		this.dueDateStart = dueDateStart;
	}

	public LocalDate getDueDateEnd() {
		return dueDateEnd;
	}

	public void setDueDateEnd(LocalDate dueDateEnd) {
		this.dueDateEnd = dueDateEnd;
	}

}
