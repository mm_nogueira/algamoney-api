package com.example.algamoney.api.repository.filter;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PersonFilter {

	private Long id;
	private String name;
	
}
