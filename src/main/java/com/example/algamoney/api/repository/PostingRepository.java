package com.example.algamoney.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.algamoney.api.model.Posting;
import com.example.algamoney.api.repository.posting.PostingRepositoryQuery;

public interface PostingRepository extends JpaRepository<Posting, Long>, PostingRepositoryQuery {

}
