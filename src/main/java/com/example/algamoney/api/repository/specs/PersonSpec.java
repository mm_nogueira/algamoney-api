package com.example.algamoney.api.repository.specs;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.Assert;

import com.example.algamoney.api.model.Person;
import com.example.algamoney.api.repository.filter.PersonFilter;


public class PersonSpec {

	private static final String LIKE_MATCHER = "%";
	
	public static Specification<Person> byUserFilter(final PersonFilter filter, String fieldOrder) {
		Assert.notNull(filter, "filter can't be null");

		return (Root<Person> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
			query.distinct(true).orderBy(cb.asc(root.get(fieldOrder)));
			return cb.and(constructUserPredicates(filter, cb, root));
		};

	}

	/**
	 * Construct predicate's List
	 * @return The predicate array
	 */
	private static Predicate[] constructUserPredicates(final PersonFilter filter, final CriteriaBuilder cb, final Root<Person> root) {
		final List<Predicate> predicates = new ArrayList<>();
		
		// id 
		Optional.ofNullable(filter.getId()).ifPresent(id -> predicates.add(cb.equal(cb.upper(root.get("id")),id)));
		
		// name 
		Optional.ofNullable(filter.getName()).ifPresent(name -> predicates.add(cb.like(cb.upper(root.get("name")), likeConditional(name))));
		
		return predicates.toArray(new Predicate[] {});
	}
	
	private static String likeConditional(final String field) {
		return LIKE_MATCHER + field.toUpperCase() + LIKE_MATCHER;
	}

}
