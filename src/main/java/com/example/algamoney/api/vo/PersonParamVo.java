package com.example.algamoney.api.vo;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@SuppressWarnings("unused")
public class PersonParamVo {

	public static final int DEFAULT_OFFSET = 0;
	public static final int DEFAULT_LIMIT = 10;

	@Min(0)
	private Integer offset = DEFAULT_OFFSET;

	@Min(0)
	@Max(50)
	private Integer limit = DEFAULT_LIMIT;

	private Long id;
	private String name;
}
