package com.example.algamoney.api.vo;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PersonVo {

	private Long id;
	private String name;
	
}
